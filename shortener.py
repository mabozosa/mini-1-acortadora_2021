#!/usr/bin/python3

import webapp
from urllib.parse import unquote


dicc_urls = {}  # Diccionario con clave el recurso y valor la url


formulario = """
 <form action ="" method ="POST">
    Escriba la URL a acortar:<br>
    <input type="text" name="url"><br><br>
    Escriba el recurso acortado:<br>
    <input type="text" name="short"><br><br>
    <input type="submit" value="Enviar"><br>
 </form>
"""


class shortener_URLs(webapp.webApp):
    def parse(self, request):
        method, resource, _ = request.split(' ', 2)  # Trocea con espacio 2 veces
        body = request.split('\r\n\r\n', 1)[1]  # Trocea /r/n/r/n 1 vez y se queda con el cuerpo
        return method, resource, body

    def process(self, parsedRequest):
        method, resource, body = parsedRequest
        print('\r\nMétodo: ' + method, '\r\nRecurso: ' + resource, '\r\nBody: ' + body)

        if method == "GET":
            if resource == "/":  # GET / para el recurso principal
                return ("200 OK",
                        "<html><body><h2>App para acortar URLs </h2><br>" +
                        "<h3>" + formulario + "</h3><br>" +
                        "Diccionario de URLs acortadas: " +
                        str(dicc_urls) + "</body></html>")
            elif resource == "/favicon.ico":  # Contemplado favicon.ico
                return ("404 Not Found", "<html><body><h3>Error. No se encuentra el recurso.</h3>" +
                        "<a href=http://localhost:1234/>App para acortar URLs</a></body></html>")
            else:
                recurso = str(resource.split("/")[1])  # El recurso de la URL sin /
                if recurso in dicc_urls.keys():  # GET /recurso para URL previamente acortadas
                    print('REDIRECT: ' + str(dicc_urls[recurso]))
                    return ("302 Found\r\nLocation: " + dicc_urls[recurso], '')
                else:  # Recursos que todavía no estan, hay que cortar antes la URL
                    return ("404 Not Found", "<html><body><h3>Error. No se encuentra el recurso.</h3>" +
                            "<a href=http://localhost:1234/>App para acortar URLs</a></body></html>")

        if method == "POST":
            short_body = unquote(body.split("&")[1])  # short=recurso si hay recurso en el formulario
            if body.find("url=&") != -1:  # Si se da esta cadena en el body es porque
                # el campo url del formulario esta vacio
                return ("400 Bad Request", "<html><body><h3>Error. Por favor, introduzca una URL en el formulario." +
                        "<br><br><a href=http://localhost:1234/>App para acortar URLs</a></h3></body></html>")
            elif short_body == "short=":  # Manejo del cuerpo vacio para formulario con campo short
                return ("400 Bad Request", "<html><body><h3>Error. Por favor, introduzca un recurso en el formulario." +
                        "<br><br><a href=http://localhost:1234/>App para acortar URLs</a></h3></body></html>")
            else:
                url_body = unquote(body.split("=")[1].split("&")[0])  # Decodifica la URL para poder trabajar con ella
                if (url_body.find("https://www.") == 0):  # Guarda URL sin www.
                    url_body = "https://" + str(url_body.split(".")[1]) + "." + str(url_body.split(".")[2])
                elif (url_body.find("http://www.") == 0):
                    url_body = "http://" + str(url_body.split(".")[1]) + "." + str(url_body.split(".")[2])
                elif (url_body.find("https://") == 0):  # se tiene en cuenta el https en la URL
                    url_body = "https://" + str(url_body.split("//")[1])
                elif (url_body.find("http://") == 0):  # se tiene en cuenta el http en la URL
                    url_body = "http://" + str(url_body.split("//")[1])
                else:  # se añade https a la URL
                    if (url_body.find("www.") == 0):
                        url_body = "https://" + str(url_body.split(".")[1]) + "." + str(url_body.split(".")[2])
                    else:
                        url_body = "https://" + url_body

                recurso_acortado = short_body.split("=")[1]  # El recurso acortado que viene en la qs
                dicc_urls[recurso_acortado] = url_body  # Almacena en el diccionario la URL introducida
                # usando como clave el recurso acortado indicado en el formulario

                return ("200 OK",
                        "<html><body><h3> La URL introducida es : " +
                        "<a href='" + str(url_body) + "'>" + str(url_body) +
                        "</a>" +
                        "</a></h3>--------------<br><h3>La URL acortada es : " +
                        "<a href='" + str(url_body) +
                        "'>" + "http://localhost:1234/" +
                        str(recurso_acortado) +
                        "</h3><br><br><a href=http://localhost:1234/>App para acortar URLs</a></body></html>")


if __name__ == "__main__":
    testWebApp = shortener_URLs("localhost", 1234)
